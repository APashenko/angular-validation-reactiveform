FROM node:16 AS development

WORKDIR /usr/src/app

# RUN apt-get update
# RUN apt-get install net-tools
# RUN apt-get install nano
# RUN apt-get -y install mc

RUN npm install -g @angular/cli@14.1.3

COPY pacage*.json ./

COPY . .

RUN npm install

# CMD ["ng", "serve", "--host", "0.0.0.0"]

