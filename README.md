# Валидация реактивной формы

https://angular.io/guide/form-validation#adding-cross-validation-to-reactive-forms

## Build

Run `make build-prod`

Open browser on `http://localhost:4200`

## Your pages are served under: 

https://apashenko.gitlab.io/angular-validation-reactiveform

## Build Dev Docker

Run `make docker-build-dev`

Open browser on `http://localhost:4000`