import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

interface errorMessage {
  [index: string]: string;
}

export const passwordMatchingValidator = (control: FormGroup): void => {
  //console.log(control);
  const password = control.get('password');
  const confirmPassword = control.get('confirmPassword');
  if (password?.value !== confirmPassword?.value)
    confirmPassword?.setErrors({ confirmPassword: true });

  //return password?.value === confirmPassword?.value ? null : { confirmPassword: true };
};

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  loginForm: FormGroup;

  errorMessage: errorMessage = {
    required: 'Поле не должно быть пустым',
    minlength: 'Минимальная длина 6 символов',
    maxlength: 'Максимальная длина 30 символов',
    email: 'E-mail адрес не корректный',
    confirmPassword: 'Поля не совпадают',
  };

  constructor(public formBuilder: FormBuilder) {
    this.loginForm = this.formBuilder.group(
      {
        fname: ['', Validators.compose([Validators.required])],
        lname: ['', Validators.compose([Validators.required])],
        email: [
          '',
          Validators.compose([Validators.required, Validators.email]),
        ],
        password: [
          '',
          Validators.compose([
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(30),
          ]),
        ],
        confirmPassword: ['', Validators.compose([Validators.required])],
      },
      { validators: this.password }
      //{ validators: passwordMatchingValidator }
      //{ validators: this.password.bind(this) }
    );
  }

  getMessageError(er: string): string {
    const d = Object(this.loginForm.controls[er].errors);
    return this.errorMessage[Object.keys(d)[0]];
  }

  password(control: FormGroup) {
    console.log(control);
    const password = control.get('password');
    const confirmPassword = control.get('confirmPassword');
    if (password?.value !== confirmPassword?.value)
      confirmPassword?.setErrors({ confirmPassword: true });
  }

  onSubmit() {
    console.log(JSON.stringify(this.loginForm.value));
  }

  ngOnInit() {}
}
