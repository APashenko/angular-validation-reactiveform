
SHELL  := /bin/bash

#COLORS
RED    := $(shell tput -Txterm setaf 1)
GREEN  := $(shell tput -Txterm setaf 2)
WHITE  := $(shell tput -Txterm setaf 7)
YELLOW := $(shell tput -Txterm setaf 3)
RESET  := $(shell tput -Txterm sgr0)

.PHONY: help
.DEFAULT_GOAL := help

# help:
# 	@echo '$(WHITE)Available the following targets$(RESET):'
# 	@echo '$(YELLOW)docker$(RESET):'
# 	@echo '  $(GREEN)restart         $(RESET) - restart docker cluster'
# 	@echo '  $(GREEN)logs            $(RESET) - show realtime logs from all containers'
# 	@echo '  $(GREEN)pull            $(RESET) - pull latest images'
# 	@echo '  $(GREEN)mysql-import    $(RESET) - import database from file "dump.sql.gz"'
# 	@echo '  $(GREEN)set-permissions $(RESET) - set permissions on folders and files"'

build-prod:
	ng build --base-href /angular-validation-reactiveform/ && git add . && git commit -am "build" && git push

docker-list-all:
	echo -e '\n\n$(GREEN)Containers$(RESET)' && docker ps -a && echo -e '\n$(GREEN)Images$(RESET)' && docker images -a && echo -e '\n$(GREEN)Volumes$(RESET)' && docker volume ls -q

remove-container:
	docker-compose down && docker rm -f app

remove-image:
	docker rmi -f annikpa/app:dev

docker-build-dev:
	docker-compose build && docker-compose up -d

remove-all:
	docker-compose down \
	&& docker rm -f app \
	&& docker system prune -a --volumes -f \
	&& docker rmi -f annikpa/app:dev \
	&& make docker-list-all
	
	
# docker volume rm -f $(docker volume ls -q)